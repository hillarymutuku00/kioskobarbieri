# Bienvenidos a #kioscobarbieri

Este es un **proyecto de simulación de una tienda virtual informática**, donde podemos encontrar productos gaming, cellphones, laptops, etc.

Utilicé una base de datos en la nube (Firebase) y agregué a la misma los productos, la compra con su respectivo formulario de datos del comprador.

Todo esto lo hice aplicando todos los conceptos vistos hasta el momento *(estilos, promise, useState, useEffect, useContext, Firebase, etc.)*.

Agregué un componente de error, por si el usuario decide ingresar a alguna página manualmente y no existe la misma.

Esperando poder mejorarlo a futuro, pudiendo agregar más contenido y conceptos que pueda adquirir con el tiempo.

Puedes hacer [click aquí](https://kiosco-barbieri.netlify.app/) para ir al sitio alojado en Netlify !

Gracias !

**Datos personales:**

 - Nombre: Luciano Barbieri
 - LinkedIn: [https://www.linkedin.com/in/luciano-barbieri-a58991218/](https://www.linkedin.com/in/luciano-barbieri-a58991218/)
 - Email: [luchobarbieri86@gmail.com](mailto:luchobarbieri86@gmail.com)
 - Instagram: [https://www.instagram.com/lucho_barbieri/](https://www.instagram.com/lucho_barbieri/)

 **Datos cursada:**
  - Plataforma: [CoderHouse](https://www.coderhouse.com/)
  - Docente: [Alejandro Fernandez](https://www.linkedin.com/in/alejandro-fernandez-755324a9/)
  - Tutor: [Maximiliano Jose Tortosa](https://www.linkedin.com/in/maximilianotortosa/)