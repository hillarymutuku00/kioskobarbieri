import { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { contexto } from "../../CartContext";
import { addDoc, collection, serverTimestamp, updateDoc, doc, increment } from "@firebase/firestore";
import { db } from "../../firebase/firebase";
import Formulario from "../Formulario/formulario"

const Cart = (props) => {
    const { cart, deleteProduct, clearCart, getProductsQty, getProductsPrice } = useContext(contexto);
    let total = getProductsPrice();
    const [idVenta, setIdVenta] = useState("");
    const [showForm, setShowForm] = useState(false);

    const handleFinalize = () =>{
        setShowForm(true)
    }

    const finalizarCompra = (comprador) => {
        const ventaCollection = collection(db, "ventas");
        addDoc(ventaCollection, {
            comprador,
            items: cart,
            data: serverTimestamp(),
            total
        })
        .then(result => {
            setIdVenta(result.id);
        })

        cart.forEach(p => {
            const updateCollection = doc(db, "ItemCollection", p.id);
            updateDoc(updateCollection, {stock: increment(-p.qty)});
        })
        clearCart();
    }

    const mostrarItems = () =>{
        return (
            <>
                <p><strong>Los siguiente productos están agregados al carrito:</strong></p>
                {
                    cart.length ?
                    cart.map((products, index) => {
                        return(
                            <div key={index}>
                                <p><strong>Nombre:</strong> {products.name} | <strong>Cantidad:</strong> {products.qty} | <strong>Precio total:</strong> {products.priceInCart} </p>
                                <button value={products.id} onClick={()=>deleteProduct(products.id)}>Remover producto</button>
                                <br/><br/>
                            </div>
                        )
                    })
                    : <p>El carrito está vacío</p>
                }
                <button onClick={clearCart}>Vaciar el carrito</button>
                <br/><br/>
                <Link to="/"><button>Volver al listado/Seguir comprando</button></Link>
            </>
        );
    };

    const irHome = () => {
        return <Link to="/"><button>Carrito vacío. Volver al listado</button></Link>
    }

    return(
        <>
            {cart.length >= 1  ?  mostrarItems() : irHome()}
            <p><strong>Cantidad Total:</strong> {getProductsQty()} {' '} <strong>Total:</strong> ${getProductsPrice()}</p>
            <button className ="checkout" onClick={handleFinalize}>COMPRAR</button>
            {showForm && <Formulario finalizarCompra={finalizarCompra}/>}
            {showForm && <strong>ID Venta: {idVenta}</strong>}
        </>
    );
}

export default Cart;