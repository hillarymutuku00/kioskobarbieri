import React from 'react';
import CartWidget from '../CartWidget/index';
import Button from '@mui/material/Button';
import 'materialize-css/dist/css/materialize.min.css';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
    const categorys = [
        { name: "Cellphones", route: "categories/cellphones", categoryId: 1 },
        { name: "Gaming", route: "categories/gaming", categoryId: 2 },
        { name: "Hardware", route: "categories/hardware", categoryId: 3 },
        { name: "Laptops", route: "categories/laptops", categoryId: 4 },
        { name: "Software", route: "categories/software", categoryId: 5 },
    ];

    return (
        <>
            <nav>      
                <div className='container'>
                    <ul id='nav-mobile' className='left hide-on-med-and-down'>
                        <li><h1>Barbieri e-commerce</h1></li>
                    </ul>
                    <ul id='nav-mobile' className='right hide-on-med-and-down'>
                        <li><NavLink to="/" style={{textDecoration: 'none'}}><Button variant="contained">HOME</Button></NavLink></li>
                        {categorys.map((element) => {
                            return (
                                <li key={element.categoryId}>
                                    <NavLink to={element.route} style={{textDecoration: 'none'}}>
                                        {element.name}
                                    </NavLink>
                                </li>
                            );
                        })}
                        <li><NavLink to="/cart"><CartWidget /></NavLink></li>
                    </ul>
                </div>
            </nav>
        </>
    );
};

export default NavBar;