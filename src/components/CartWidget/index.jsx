import React from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

const CartWidget = () => (<ShoppingCartIcon color="primary"/>);

export default CartWidget;