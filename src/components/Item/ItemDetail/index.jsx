import React, { useContext } from 'react';
import { CCard, CCardImage, CCardBody, CCardTitle, CCardText } from '@coreui/react';
import { NavLink } from 'react-router-dom';
import { contexto } from '../../../CartContext';
import ItemCount from '../ItemCount/index';

const ItemDetail = ({producto}) => {
         
    const { addProduct } = useContext(contexto);
    const item = {id: producto.id, name: producto.name, price: producto.price};
    const onAdd = (count) => {
        addProduct(item, count, item.price * count)
    }

    return (
            <>
                <CCard className="text-center" style={{ width: '18rem' }}>
                <NavLink to={{pathname: `/itemdetail/${producto.itemId}`}}>
                    <CCardImage orientation="top" src={producto.url} />
                </NavLink>
                <CCardBody>
                    <CCardTitle>{producto.name}</CCardTitle>
                    <CCardText>
                        PRECIO: {producto.price} DESCRIPCION: {producto.description} STOCK: {producto.stock}
                    </CCardText>
                </CCardBody>
                </CCard>
                <ItemCount stock={producto.stock} initial={1} onAdd={onAdd} name={producto.name} />
            </>
        )
}

export default ItemDetail;