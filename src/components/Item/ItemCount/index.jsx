import React, { useState, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { contexto } from '../../../CartContext';

const ItemCount = ({stock, initial, onAdd, name}) => {
  
    const [contador, setContador] = useState(initial);
    const [open, setOpen] = useState(false)
    const [producto, setProducto] = useState({name: name, cantidad: contador});
    const { deleteProduct } = useContext(contexto);

    const handleClick = () => {
      setOpen(!open)
    }

    const incrementaProducto = () => {
        contador < stock
            ? setContador(contador + 1)
            : setContador(contador);
    };

    const decrementaProducto = () => {
        contador > 0
        ? setContador(contador - 1)
        : setContador(contador);
    };

    const updateQuantity = () => {
        setProducto(previousState => {
            const x = {...previousState, cantidad: contador};
            return x;
        });
    }

    const agregarProducto = () => {
        if ((contador > 0) && (contador <= stock)) {
            updateQuantity();
            onAdd(contador, producto);
        }
    }

    const removeFromCart = () => deleteProduct(producto.id);

    return (
        <>            
            <h1>{contador}</h1>
            <button onClick={decrementaProducto}>-</button>
            {!open ? ( 
                <button onClick={()=> {agregarProducto(); handleClick()}}>Agregar al carrito</button>
            ) : ( 
                <NavLink to={{pathname: `/cart`}}>
                    <button onClick={handleClick}>Finalizar Compra</button>
                </NavLink>
            )}
            <button onClick={incrementaProducto}>+</button>
            <br/>
            <button onClick={removeFromCart}>Borrar producto del carrito</button>           
        </>
    )
}

export default ItemCount;