import React from 'react';
import '@coreui/coreui/dist/css/coreui.min.css';
import { CCol, CCard, CCardImage, CCardBody, CCardTitle, CCardText, CButton } from '@coreui/react';
import Item from '../Item/index';
import { NavLink } from 'react-router-dom';

const ItemList = ({arrProductos}) => { 

    const producto = arrProductos.map((prod,index) => {
        return (
                <CCol xs key={index}>
                    <CCard className="text-center" style={{ width: '18rem' }}>
                    <NavLink to={{pathname: `/itemdetail/${prod.id}`}}>
                        <CCardImage orientation="top" src={prod.url} style={{width: 250, height: 250}} />
                    </NavLink>
                    <CCardBody>
                        <CCardTitle>{prod.name}</CCardTitle>
                        <CCardText>
                        ID: {prod.id} PRECIO: {prod.price} STOCK: {prod.stock}
                        </CCardText>
                        <NavLink to={{pathname: `/itemdetail/${prod.id}`}}>
                            <CButton>VER DESCRIPCION</CButton>
                        </NavLink>
                    </CCardBody>
                    </CCard>
                </CCol>
        );
    });

    return (
            <Item producto={producto} />
    )
}

export default ItemList;