import React, { createContext, useState } from 'react';

export const contexto = createContext();
const { Provider } = contexto;

const CustomProvider = ({ children }) => {
    const [cart, setCart] = useState([]);

    const addProduct = ( product, qty, priceInCart ) => {
        const newProduct = {
            ...product,
            qty,
            priceInCart
        };
        if (cart.find(obj => obj.id === product.id)) {
            const productFind = cart.find(product => product.id === newProduct.id);
            const index = cart.indexOf(productFind);
            const aux = [...cart];
            aux[index].qty += qty;
            setCart(aux);
        } else {
            setCart([...cart, newProduct]);
        }
    };

    const isInCart = ( product ) => cart.find(obj => obj.id === product.id);

    const deleteProduct = (id) =>{      
        const productoEncontrado = cart.findIndex((e)=> e.id === id)
        if(productoEncontrado !== -1){
            cart.splice(productoEncontrado,1);
            setCart([...cart]);
        }
    }

    const clearCart = () => {
        setCart([]);
    }

    const getProductsQty = () => {
        return cart.reduce((total, p) => (total += p.qty), 0);
    };
    
    const getProductsPrice = () => {
        return cart.reduce((total , p) => (total += p.price * p.qty), 0);
    };
    
    const ContextValue = {
        cart,
        addProduct,
        deleteProduct,
        isInCart,
        getProductsQty,
        getProductsPrice,
        clearCart
    }

    return(
        <Provider value={ContextValue}>
            {children}
        </Provider>
    )
};

export default CustomProvider;