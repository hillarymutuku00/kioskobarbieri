import { useEffect, useState } from "react";
import { doc, getDoc, collection } from '@firebase/firestore';
import { useParams } from "react-router-dom";
import ItemDetail from "../../components/Item/ItemDetail/index";
import { ClipLoader } from 'react-spinners';
import { db } from "../../firebase/firebase";

const ItemDetailContainer = () => {
   
    const [producto, setProducto] = useState([]);
    const [loading, setLoading] = useState(true);
    const { id } = useParams();
    const style = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)" };

    useEffect(() => {        
        const productsCollection = collection(db, "ItemCollection");
        const refDoc = doc(productsCollection, id);
        getDoc(refDoc)
        .then((result) => {
            const product = {
                id,
                ...result.data()
            }
            setProducto(product);
        })
        .catch((error) => {
            console.error(error);
        })
        .finally(() => {
            setLoading(false);
        })
    }, [id]);

return (
    <>
        {loading ? (
            <span style={style}>
                <ClipLoader color={'red'} loading={loading} size={150} /> 
            </span>        
        ): ( 
        <ItemDetail producto={producto} />
        )}
    </>
)}

export default ItemDetailContainer;