import React, { useEffect, useState } from 'react';
import { getDocs, collection, query, where, orderBy } from '@firebase/firestore';
import 'materialize-css/dist/css/materialize.min.css';
import { CRow } from '@coreui/react';
import { useParams } from 'react-router-dom';
import { ClipLoader } from 'react-spinners';
import ItemList from '../../components/Item/ItemList/index';
import { db } from "../../firebase/firebase";

const ItemListContainer = ({greeting}) => {

    const [productos, setProductos] = useState([]);
    const [loading, setLoading] = useState([true]);  
    const {name} = useParams();

    useEffect(() => {        
        const productsCollection = collection(db, "ItemCollection");
        let q;
        if(name === undefined){
            q = query(productsCollection, orderBy("category"), orderBy("name"));
        }else{
            q = query(productsCollection, where("category", "==", name));
        }
        getDocs(q)
        .then((result) => {
            const docs = result.docs;
            const lista = docs.map((producto) => {
                const id = producto.id
                const product = {
                    id,
                    ...producto.data()
                }
                return product
            })
            setProductos(lista);
        })
        .catch(error => {
            console.error(error)
        })
        .finally(()=>  {
            setLoading(false);
        })
    }, [name]);

    const style = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)" };

    return (
        <>
            <p className="flow-text">{greeting}</p>
            {loading ? (
                <span style={style}>
                    <ClipLoader color={'red'} loading={loading} size={150} />    
                </span>
            ) : ( 
                <CRow xs={{ cols: 1, gutter: 4 }} md={{ cols: 3 }}>
                    <ItemList arrProductos={productos} /> 
                </CRow>      
            )}
        </>
    )
}

export default ItemListContainer;